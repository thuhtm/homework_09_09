<?php

class Circle {
    public $bankinh;
    public function __construct($bankinh){
        $this->bankinh=$bankinh;
    }
    public function tinh_chu_vi(){
        $kq= ($this->bankinh * 2) * 3.14;
        return number_format($kq, 2);
    }
    public function tinh_dien_tich(){
        $kq= ($this->bankinh * $this->bankinh) * 3.14;
        return number_format($kq, 2);
    }
}
if(isset($argv[1]) ){
    $circle = new Circle($argv[1]);
    echo "Chu Vi: ";
    echo $circle->tinh_chu_vi();
    echo "\n";
    echo "Dien tich: ";
    echo $circle->tinh_dien_tich();
    echo "\n";
}
else{
    echo "Ban chu nhap value";
    echo "\n";
}
