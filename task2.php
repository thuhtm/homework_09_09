<?php
    //In ra tất cả các số lặp lại trên 3 lần từ file input.txt
    $myfile = fopen("input.txt", "r") or die("Unable to open file!");
    //Xoá các khoảng cách dòng và đọc dữ liệu từ file input.txt
    $text_array = preg_split("/[\s,]+/",fread($myfile,filesize("input.txt")));
    //Đếm dữ liệu
    $count = array_count_values($text_array);
    //Thêm kết quả vào mảng
    $number_count = array();
    foreach ($count as $key=>$value){
        if($value >= 3){
            array_push($number_count,$key);// push dữ liệu vào mảng
        }
    }
    $output = implode(',',$number_count);// gộp phần tử  mảng thành chuỗi
    echo 'String repeats 3 or more times:'.$output;
    echo "\n";
?>